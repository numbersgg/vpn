# VPN Setup

### Prerequisite

- Already have an existing `.ovpn` file.


### Installation

1. Copy the `.env.example`

2. Update the config value.
    ```bash
    echo <path/to/ovpn> | base64 -w 0
    ```
    copy output to the `CONFIG` variable

3. 
    ```bash
    docker-compose up
    ```

4. Configure browser to connect to Socks5 Proxy, on `localhost:1080`
    ```text
    socks5://localhost:1080
    ``` 