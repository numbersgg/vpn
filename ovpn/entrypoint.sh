#!/bin/bash

if [ -z ${CONFIG+x} ]; then
  echo "No OpenVPN config environment variable \$CONFIG found, using default files...";
else
  echo $CONFIG | base64 -d > /etc/openvpn/default.conf
  if [ "$USE_TOR" == "true" ]; then
    echo "using TOR...";
    echo "
socks-proxy-retry
socks-proxy tor 9150
" >> /etc/openvpn/default.conf;
  fi
fi

cd /etc/openvpn && /usr/sbin/openvpn --config *.conf --script-security 2 --up /usr/local/bin/sockd.sh
